#!/usr/bin/env python
# ==============================================================================
#  File and Version Information:
#       $Id$
#
#  Description:
#       This example shows a simple unfolding scenario. Truth data is generated
#       according to a bimodal p.d.f. and then simulates detector response on
#       these events in a naive way by applying a simple smearing and 
#       efficiency function. The resulting truth and reco level events are
#       then used to build a response matrix that can be used for unfolding.
#       The to-be unfolded data is simulated by Poisson smearing a disjoint
#       reco level dataset. This data is then unfolded with a simple matrix 
#       inversion.
#
#  Author: Pim Verschuuren <pim.verschuuren@rhul.ac.uk>
#
# ==============================================================================

dict_data = {}        
                      
# Truth/Reco bin count.  
dict_data['tbins'] = 30  
dict_data['rbins'] = 30  

# Kinematic ranges. (delta eta)
dict_data['xmin'] = -4         
dict_data['xmax'] = 4          
                      
# Smearing parameters.
dict_data['bias'] = 1.1
dict_data['sigma'] = 0.85

# Efficiency type
#dict_data['eff'] = 'lin'
dict_data['eff'] = 'quad'
                         
# Bimodal assymmetry.
dict_data['frac'] = 0.4  
                                                  
# Overflow flag.         
overflow = True          
                                                        
# Number of events.            
dict_data['nevents'] = 5000    
                               
# How much more MC do we have than data
dict_data['mcLumiFactor'] = 10         
                                       
# Constant background bin count        
dict_data['nbkg'] = 5 



from helpers import *



def main():

  import ROOT

  # Prepare the histograms and response matrix.
  histograms = prepare_bimodal(dict_data)

  # Get the truth distribution for training.
  train_truth = histograms["truth_test"]
  
  # Get the reco distribution for training.
  train_reco = histograms["reco_test"]
  
  # Get the to-be unfolded data distribution.
  data = histograms["data"]

  # Get the response matrix.
  response = histograms["response"]

  # This plots the simulated data and corresponding
  # response matrix, bin purities and efficiences.
  plot_input(train_truth, train_reco, data, response, histograms["purity"], histograms["eff"])



  # return # ===== Comment this return to continue ===== #



  # Create a spectator object.
  spec = ROOT.RooUnfoldSpec("unfold","unfold",train_truth,"obs_truth",train_reco,"obs_reco",response,0,data,overflow,0.0005)  
  func = spec.makeFunc(ROOT.RooUnfolding.kInvert)

  # Get the histogram to compare the results with.
  test_truth = histograms["truth_test"]

  # Create a RooFitHist object as input for the unfolding
  # results printing.
  test_truth_RFH = spec.makeHistogram(test_truth)

  # Initiate the unfolding by requesting the
  # table with unfolded results.
  func.unfolding().PrintTable(ROOT.cout, test_truth_RFH)
  

  # Plot the results.
  plot_result(test_truth, func, True)
  

if __name__=="__main__":
  main()
  
